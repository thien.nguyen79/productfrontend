import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogExampleComponent } from './dialog-example/dialog-example.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'first-app';
  constructor(public dialog: MatDialog) { }

  // openDialog() {

  //     // const dialogConfig = new MatDialogConfig();

  //     // dialogConfig.disableClose = true;
  //     // dialogConfig.autoFocus = true;

  //     this.dialog.open(DialogExampleComponent);
  // }
}
