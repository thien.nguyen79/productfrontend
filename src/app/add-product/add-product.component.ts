import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Product } from '../product';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  constructor(private productservice: ProductService) { }

  product: Product = new Product();
  submitted = false;

  ngOnInit() {
    this.submitted = false;
  }

  productsaveform = new FormGroup({
    product_name: new FormControl(),
    remained_number: new FormControl(),
    product_branch: new FormControl()
  });

  saveProduct(saveProduct) {
    console.log(this.ProductName.value);
    console.log("fake");

    this.product = new Product();
    this.product.productName = this.ProductName.value;
    this.product.remainedNumber = this.RemainedNumber.value;
    this.submitted = true;
    this.save();
  }

  save() {
    this.productservice.createProduct(this.product)
      .subscribe(data => console.log(data), error => console.log(error));
    this.product = new Product();
  }

  get ProductName() {
    return this.productsaveform.get('product_name');
  }

  get RemainedNumber() {
    return this.productsaveform.get('remained_number');
  }

  addProductForm() {
    this.submitted = false;
    this.productsaveform.reset();
  }
}  