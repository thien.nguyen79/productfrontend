import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductListComponent } from '../product-list/product-list.component'
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable, Subject } from "rxjs";
import { ProductService } from '../product.service';
import { Product } from '../product';

import { MatDialog } from '@angular/material/dialog';

@Component({
  providers: [ProductListComponent],
  selector: 'app-dialog-example',
  templateUrl: './dialog-example.component.html',
  styleUrls: ['./dialog-example.component.scss']
})
export class DialogExampleComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog,
    private productservice: ProductService) {
    console.log(data.product.productId);
  }


  products: Observable<Product[]>;
  product: Product = new Product();
  isUpdate = false;
  ngOnInit(): void {
  }

  productsaveform = new FormGroup({
    productId: new FormControl(),
    productName: new FormControl(),
    remainedNumber: new FormControl()
  });
  //   openDialog() {

  //     // const dialogConfig = new MatDialogConfig();

  //     // dialogConfig.disableClose = true;
  //     // dialogConfig.autoFocus = true;

  //     this.dialog.open(DialogExampleComponent);
  // }


  updatePro() {
    this.product = new Product();
    this.product.productName = this.ProductName.value;
    this.product.remainedNumber = this.RemainedNumber.value;

    this.productservice.updateProduct(this.ProductId.value, this.product).subscribe(
      data => {
        this.productservice.getProductList().subscribe(data => {
          this.products = data
          this.isUpdate = true;
        })
      },
      error => console.log(error));
  }
  refreshPage() {
    // new ProductListComponent(this.productservice, this.dialog);
  }
  get ProductName() {
    return this.productsaveform.get('productName');
  }

  get RemainedNumber() {
    return this.productsaveform.get('remainedNumber');
  }
  get ProductId() {
    return this.productsaveform.get('productId');
  }
}
