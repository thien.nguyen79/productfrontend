import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = 'http:localhost:8081/api'
  constructor(private http: HttpClient) { }

  getProductList(): Observable<any> {
    return this.http.get(`http://localhost:8081/api/products`);
  }

  createProduct(product: object): Observable<object> {
    return this.http.post('http://localhost:8081/api/product', product);
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(`http://localhost:8081/api/product/${id}`, { responseType: 'json' });
  }

  getProduct(id: number): Observable<any> {
    return this.http.get(`http://localhost:8081/api/product/${id}`, { responseType: 'json' });
  }

  updateProduct(id: number, value: any): Observable<Object> {
    return this.http.put(`http://localhost:8081/api/product/${id}`, value);
  }
}
