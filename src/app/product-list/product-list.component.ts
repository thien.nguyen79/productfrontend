import { Component, OnInit, NgModule, Input, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Observable, Subject } from "rxjs";
import { ProductService } from '../product.service';
import { Product } from '../product';
import { MatDialog } from '@angular/material/dialog';
import { DialogExampleComponent } from '../dialog-example/dialog-example.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  constructor(private productservice: ProductService,
    public dialog: MatDialog) { }

  productsArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  products: Observable<Product[]>;
  product: Product = new Product();
  deleteMessage = false;
  productlist: any;
  isUpdate = false;
  ngOnInit() {
    this.dtOptions = {
      pageLength: 6,
      stateSave: true,
      lengthMenu: [[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };

    this.productservice.getProductList().subscribe(data => {
      // console.log(this.isUpdate); check key search

      this.products = data;
      this.dtTrigger.next();
    })
  }

  async openDialog(id: number) {
    const response = await fetch(`http://localhost:8081/api/product/${id}`);
    const json = await response.json();
    // console.log(json);
    this.dialog.open(DialogExampleComponent, {
      data: { product: json }
    }).afterClosed()
      .subscribe(() =>
        this.ngOnInit()
      );
  }

  searchProduct(key: any) {
    console.log(key);

  }

  deleteProduct(id: number) {
    this.productservice.deleteProduct(id)
      .subscribe(
        data => {
          console.log(this.product.productId);
          this.deleteMessage = true;
          this.productservice.getProductList().subscribe(data => {
            this.products = data
          })
        },
        error => console.log(error));
  }
}  
